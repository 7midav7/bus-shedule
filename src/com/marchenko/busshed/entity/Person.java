package com.marchenko.busshed.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Marchenko Vadim on 9/28/2015.
 */
public class Person extends Thread{
    public enum StatePerson {SEARCHING, WAITING,  RIDING, FINISHED}
    private static final Logger LOGGER = LogManager.getLogger(Person.class);
    private static final int MAX_SIZE_CROWD_PERSONS = 100_000;

    private int idPerson;
    private int currentStopId;
    private int plannedStopId;
    private int finishStopId;
    private int chosenBusId;
    private StatePerson statePerson = StatePerson.SEARCHING;

    public Person(int idPerson, int currentStopId, int finishStopId) {
        this.idPerson = idPerson;
        this.currentStopId = currentStopId;
        this.finishStopId = finishStopId;
    }

    public int getIdPerson() {
        return idPerson;
    }

    @Override
    public void run() {
        while (statePerson != StatePerson.FINISHED){
            switch (statePerson){
                case WAITING: waiting(); break;
                case RIDING: riding(); break;
                case SEARCHING: searchingRoute(); break;
                case FINISHED: break;
                default:
                    LOGGER.error("Undefined statement of personThread" + idPerson);
            }
        }
    }

    private void riding(){
        Bus bus = DispatchingStation.getInstance().chooseBus(chosenBusId);
        int index = bus.currentIndexStopSchedule();
        int idBusStop = bus.chooseStopSchedule(index);
        if (idBusStop == plannedStopId){
            boolean isSuccessfulExit = DispatchingStation.getInstance()
                    .chooseBus(chosenBusId).removePerson(this);
            if (isSuccessfulExit){
                if (plannedStopId == finishStopId){
                    statePerson = StatePerson.FINISHED;
                } else {
                    currentStopId = plannedStopId;
                    statePerson = StatePerson.SEARCHING;
                }
            }
        }
    }

    private void waiting(){
        Stop stop = DispatchingStation.getInstance().chooseStop(currentStopId);
        if (stop.containArrivedBus(chosenBusId)){
            boolean wasAdded = DispatchingStation.getInstance().chooseBus(chosenBusId).addPerson(this);
            if (wasAdded){
                int indexBus = findIndexBusSchedule(stop, chosenBusId);
                stop.decreaseSizeCrowd(indexBus);
                statePerson = StatePerson.RIDING;
            }
        }
    }

    private int findIndexBusSchedule(Stop stop, int busId){
        int index = 0;
        while (index < stop.sizeScheduleBuses()){
            if (stop.chooseBusSchedule(index) == busId){
                break;
            }
            ++ index;
        }
        return index;
    }

    private void searchingRoute(){
        if (currentStopId == finishStopId){
            statePerson = StatePerson.FINISHED;
            return;
        }
        Stop stop = DispatchingStation.getInstance().chooseStop(currentStopId);
        int index = findIndexBusSetPlannedStop();
        stop.increaseSizeCrowd(index);
        chosenBusId = stop.chooseBusSchedule(index);
        statePerson = StatePerson.WAITING;
    }

    private int findIndexBusSetPlannedStop(){
        int chosenBusIndex = chooseBestIndexBusContains(finishStopId);
        if (chosenBusIndex == -1){
            chosenBusIndex = chooseBestIndexBusContains(0);
            plannedStopId = 0;
        } else {
            plannedStopId = finishStopId;
        }
        return  chosenBusIndex;
    }

    private int chooseBestIndexBusContains(int plannedStop){
        int index = -1;
        int minSize = MAX_SIZE_CROWD_PERSONS;

        Stop stop = DispatchingStation.getInstance().chooseStop(currentStopId);
        for (int i = 0; i < stop.sizeScheduleBuses(); ++ i){
            int idBusSchedule = stop.chooseBusSchedule(i);
            Bus bus = DispatchingStation.getInstance().chooseBus(idBusSchedule);
            if (containsStop(bus, plannedStop) && minSize > stop.sizeCrowd(i)){
                index = i;
                minSize = stop.sizeCrowd(i);
            }
        }
        return index;
    }

    private boolean containsStop(Bus bus, int finStation){
        boolean result = false;
        int index = 0;
        while (index < bus.sizeScheduleStops()){
            int idStop = bus.chooseStopSchedule(index);
            if (idStop == finStation){
                result = true;
                break;
            }
            ++ index;
        }
        return result;
    }
}
