package com.marchenko.busshed.entity;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Marchenko Vadim on 9/28/2015.
 */
public class Stop {
    private int idBusStop;
    private ArrayList<Integer> scheduleBuses;
    private CopyOnWriteArrayList<AtomicInteger> sizesCrowds = new CopyOnWriteArrayList<>();
    private ConcurrentSkipListSet<Integer> arrivedBuses = new ConcurrentSkipListSet<>();
    private Semaphore semaphore;

    public Stop(int idBusStop, int capacity, ArrayList<Integer> scheduleBuses) {
        this.idBusStop = idBusStop;
        this.semaphore = new Semaphore(capacity);
        this.scheduleBuses = scheduleBuses;
        for (int i = 0; i < scheduleBuses.size(); ++ i){
            sizesCrowds.add(new AtomicInteger(0));
        }
    }

    public int getIdBusStop() {
        return idBusStop;
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }

    public int sizeScheduleBuses(){
        return scheduleBuses.size();
    }

    public Integer chooseBusSchedule(int busIndex){
        return scheduleBuses.get(busIndex);
    }

    public void addArrivedBus(int id){
        arrivedBuses.add(id);
    }

    public void removeArrivedBus(int id){
        arrivedBuses.remove(id);
    }

    public void increaseSizeCrowd(int indexBus){
        AtomicInteger integer = sizesCrowds.get(indexBus);
        integer.addAndGet(1);
    }

    public int sizeCrowd(int indexBus){
        return sizesCrowds.get(indexBus).get();
    }

    public void decreaseSizeCrowd(int indexBus){
        AtomicInteger integer = sizesCrowds.get(indexBus);
        integer.addAndGet(-1);
    }

    public boolean containArrivedBus(Object o){
        return arrivedBuses.contains(o);
    }
}
