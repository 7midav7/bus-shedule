package com.marchenko.busshed.entity;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Marchenko Vadim on 9/30/2015.
 */
public class DispatchingStation {
    private static DispatchingStation dispatchingStation;
    private static AtomicBoolean isInitialize = new AtomicBoolean(false);
    private static Lock lock = new ReentrantLock();
    private static ArrayList<Stop> stopsForInitialization;
    private static ArrayList<Bus> busesForInitialization;

    private ArrayList<Stop> stops;
    private ArrayList<Bus> buses;

    private DispatchingStation(){
        this.stops = stopsForInitialization;
        this.buses = busesForInitialization;
    }

    public static void setStopsForInitialization(ArrayList<Stop> stopsForInitialization) {
        DispatchingStation.stopsForInitialization = stopsForInitialization;
    }

    public static void setBusesForInitialization(ArrayList<Bus> busesForInitialization) {
        DispatchingStation.busesForInitialization = busesForInitialization;
    }

    public static DispatchingStation getInstance(){
        if (!isInitialize.get()){
            try {
                lock.lock();
                if (dispatchingStation == null) {
                    dispatchingStation = new DispatchingStation();
                }
                isInitialize.set(true);
            } finally {
                lock.unlock();
            }
        }
        return dispatchingStation;
    }

    public Bus chooseBus(int index){
        return dispatchingStation.buses.get(index);
    }

    public Stop chooseStop(int index){
        return dispatchingStation.stops.get(index);
    }

    public int sizeBuses(){
        return dispatchingStation.stops.size();
    }

    public int sizeStops(){
        return dispatchingStation.stops.size();
    }
}
