package com.marchenko.busshed.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.TreeSet;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Marchenko Vadim on 9/28/2015.
 */
public class Bus extends Thread {
    private static final Logger LOGGER = LogManager.getLogger(Bus.class);

    private int idBus;
    private int capacity;
    private int timeWaiting;
    private int timeRiding;
    private AtomicInteger currentIndexStopSchedule;
    private TreeSet<Integer> passengers = new TreeSet<>();
    private ArrayList<Integer> scheduleStops = new ArrayList<>();
    private Lock doorLock = new ReentrantLock();
    private Lock passengersLock = new ReentrantLock();

    public Bus(int idBus, int capacity, int timeRiding, int timeWaiting, ArrayList<Integer> scheduleStops) {
        this.idBus = idBus;
        this.capacity = capacity;
        this.timeRiding = timeRiding;
        this.timeWaiting = timeWaiting;
        this.currentIndexStopSchedule = new AtomicInteger(0);
        this.scheduleStops.add(0);
        this.scheduleStops.addAll(scheduleStops);
    }

    public int getIdBus() {
        return idBus;
    }

    public int chooseStopSchedule(int indexStop) {
        return scheduleStops.get(indexStop);
    }

    public int currentIndexStopSchedule() {
        return currentIndexStopSchedule.get();
    }

    public int sizeScheduleStops() {
        return scheduleStops.size();
    }

    public boolean addPerson(Person person) {
        boolean entered = false;
        boolean wasAdded = false;
        try{
            passengersLock.lock();
            int numberPassengers = passengers.size();
            entered = doorLock.tryLock(0, TimeUnit.MILLISECONDS);
            if (entered && numberPassengers < capacity) {
                wasAdded = passengers.add(person.getIdPerson());
                if (!wasAdded){
                    LOGGER.warn("Trying to add exist person#" + person.getId() + "to bus#" + idBus);
                }
            }
        } catch (InterruptedException e) {
            LOGGER.error("Bus was interrupted in addPerson", e);
        } finally {
            passengersLock.unlock();
            if (entered){
                doorLock.unlock();
            }
        }
        return entered && wasAdded;
    }

    public boolean removePerson(Person person) {
        boolean exited = false;
        boolean wasRemoved = false;
        try{
            passengersLock.lock();
            exited = doorLock.tryLock(0, TimeUnit.MILLISECONDS);
            if (exited) {
                wasRemoved = passengers.remove(person.getIdPerson());
                if (!wasRemoved) {
                    LOGGER.warn("Trying to delete not existing person from bus" + person);
                }
            }
        } catch (InterruptedException e) {
            LOGGER.error("Bus was interrupted in removePerson", e);
        } finally {
            passengersLock.unlock();
            if (exited){
                doorLock.unlock();
            }
        }
        return exited && wasRemoved;
    }

    @Override
    public void run() {
        while (true) {
            Semaphore stopSemaphore = riding();
            stopped(stopSemaphore);
        }
    }

    private Semaphore riding() {
        int indexStop = currentIndexStopSchedule.get();
        int idStop = scheduleStops.get(indexStop);
        Stop stop = DispatchingStation.getInstance().chooseStop(idStop);
        Semaphore semaphore = stop.getSemaphore();

        try {
            doorLock.lock();
            Thread.sleep(timeRiding);
            semaphore.acquire();
        } catch (InterruptedException e){
            LOGGER.error("Bus was interrupted in riding", e);
        } finally {
            doorLock.unlock();
        }

        return semaphore;
    }

    private void stopped(Semaphore busStopSemaphore) {
        try {
            int idBusStop = scheduleStops.get(currentIndexStopSchedule.get());
            Stop stop = DispatchingStation.getInstance().chooseStop(idBusStop);

            stop.addArrivedBus(idBus);
            Thread.sleep(timeWaiting);
            stop.removeArrivedBus(idBus);

            int newValueIndex = (currentIndexStopSchedule.get() + 1) % scheduleStops.size();
            currentIndexStopSchedule.set(newValueIndex);
        } catch (InterruptedException e){
            LOGGER.error("Interrupted in waiting stage", e);
        } finally {
            busStopSemaphore.release();
        }
    }
}
