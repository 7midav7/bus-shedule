package com.marchenko.busshed.demonstrator;

import com.marchenko.busshed.creator.BusCreator;
import com.marchenko.busshed.creator.StopCreator;
import com.marchenko.busshed.creator.PersonCreator;
import com.marchenko.busshed.entity.Bus;
import com.marchenko.busshed.entity.Stop;
import com.marchenko.busshed.entity.DispatchingStation;
import com.marchenko.busshed.entity.Person;

import java.util.ArrayList;

/**
 * Created by Marchenko Vadim on 9/29/2015.
 */
public class Demonstrator {
    private static final int NUMBER_BUS_STOP = 9;
    private static final int NUMBER_PERSONS = 100;

    public void demonstrate(){
        BusCreator busCreator = new BusCreator();
        PersonCreator personCreator = new PersonCreator();
        StopCreator stopCreator = new StopCreator();

        ArrayList<Person> persons = personCreator.create(NUMBER_PERSONS, NUMBER_BUS_STOP);
        ArrayList<Bus> buses = busCreator.create();
        ArrayList<Stop> stops = stopCreator.create(NUMBER_BUS_STOP, buses);
        DispatchingStation.setBusesForInitialization(buses);
        DispatchingStation.setStopsForInitialization(stops);

        persons.stream().forEach(Thread :: start);
        buses.stream().forEach(p -> p.setDaemon(true));
        buses.stream().forEach(Thread::start);
    }
}
