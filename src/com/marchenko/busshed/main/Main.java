package com.marchenko.busshed.main;

import com.marchenko.busshed.demonstrator.Demonstrator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Marchenko Vadim on 9/28/2015.
 */
public class Main {
    private static final Logger LOGGER;
    private static final String LOGS_CONFIG_URI = "config\\log4j2.xml";

    static{
        System.setProperty("log4j.configurationFile", LOGS_CONFIG_URI);
        LOGGER = LogManager.getLogger(Main.class);
    }

    public static void main(String[] args) {
        Demonstrator demonstrator = new Demonstrator();
        demonstrator.demonstrate();
    }
}
