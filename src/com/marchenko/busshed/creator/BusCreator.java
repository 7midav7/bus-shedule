package com.marchenko.busshed.creator;

import com.marchenko.busshed.entity.Bus;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Marchenko Vadim on 9/28/2015.
 */
public class BusCreator {
    private static final int TIME_WAITING = 50;
    private static final int TIME_RIDING = 50;
    private static final int BUS_CAPACITY = 40;

    public ArrayList<Bus> create(){
        ArrayList<Bus> buses = new ArrayList<>();

        ArrayList<Integer> scheduleStops = new ArrayList<>(Arrays.asList(1, 3, 4, 5, 7));
        buses.add(new Bus(0, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        buses.add(new Bus(1, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        buses.add(new Bus(2, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        buses.add(new Bus(3, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        buses.add(new Bus(4, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        buses.add(new Bus(5, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        buses.add(new Bus(6, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        buses.add(new Bus(7, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        buses.add(new Bus(8, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        buses.add(new Bus(9, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        buses.add(new Bus(10, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        scheduleStops = new ArrayList<>(Arrays.asList(2, 4, 6, 8));
        buses.add(new Bus(11, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        scheduleStops = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        buses.add(new Bus(12, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));
        scheduleStops = new ArrayList<>(Arrays.asList(5, 6, 7, 8));
        buses.add(new Bus(13, BUS_CAPACITY, TIME_WAITING, TIME_RIDING, scheduleStops));

        return buses;
    }
}
