package com.marchenko.busshed.creator;

import com.marchenko.busshed.entity.Bus;
import com.marchenko.busshed.entity.Stop;

import java.util.ArrayList;

/**
 * Created by Marchenko Vadim on 9/28/2015.
 */
public class StopCreator {
    private static final int CAPACITY_BUS_STOP = 5;

    public ArrayList<Stop> create(int number, ArrayList<Bus> buses){
        ArrayList<Stop> stops = new ArrayList<>();
        ArrayList< ArrayList<Integer> > schedulesBuses = new ArrayList<>();
        for(int i = 0; i < number; ++ i){
            schedulesBuses.add(new ArrayList<>());
        }

        for (Bus bus : buses){
            for (int index = 0; index < bus.sizeScheduleStops(); ++ index){
                int numb = bus.chooseStopSchedule(index);
                schedulesBuses.get(numb).add(bus.getIdBus());
            }
        }

        for (int i = 0; i < number; ++ i){
            stops.add( new Stop(i, CAPACITY_BUS_STOP, schedulesBuses.get(i)) );
        }

        return stops;
    }
}
