package com.marchenko.busshed.creator;

import com.marchenko.busshed.entity.Person;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Marchenko Vadim on 9/28/2015.
 */
public class PersonCreator {
    public ArrayList<Person> create(int number, int maximumNumberBusStop){
        ArrayList<Person> persons = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < number; ++ i){
            Person per = new Person(i, random.nextInt(maximumNumberBusStop),
                    random.nextInt(maximumNumberBusStop));
            persons.add(per);
        }

        return persons;
    }
}
